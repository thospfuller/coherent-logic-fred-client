package com.coherentlogic.fred.client;

/**
 * This class contains module-specific constants.
 *
 * @author <a href="support@coherentlogic.com">Support</a>
 */
public class Constants {

    public static final String SERVICE = "service", ID = "id";

}
