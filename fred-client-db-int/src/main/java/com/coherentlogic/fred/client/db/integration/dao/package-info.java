/**
 * This package contains classes that follow the data access pattern and allow
 * create, read, update, and delete operations to be performed on the FRED
 * Client's domain model.
 */
package com.coherentlogic.fred.client.db.integration.dao;